package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
  private long id;
  private String numberOfRooms;
  private int price;
  private int area;
  private boolean balcony;
  private boolean loggia;
  private int floor;
  private int floorsInHouse;

  public Apartment(final Apartment apartment) {
    this.id = apartment.id;
    this.numberOfRooms = apartment.numberOfRooms;
    this.price = apartment.price;
    this.area = apartment.area;
    this.balcony = apartment.balcony;
    this.loggia = apartment.loggia;
    this.floor = apartment.floor;
    this.floorsInHouse = apartment.floorsInHouse;
  }
}
