package org.example.search;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SearchRequest {
    private String[] numberOfRooms;
    private Integer priceMin;
    private Integer priceMax;
    private Integer areaMin;
    private Integer areaMax;
    private boolean balcony;
    private boolean loggia;
    private Integer floorMin;
    private Integer floorMax;
    private Integer floorsInHouseMin;
    private Integer floorsInHouseMax;

    public static SearchRequest requestByArea(Integer areaMin, Integer areaMax) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setAreaMin(areaMin);
        searchRequest.setAreaMax(areaMax);
        return searchRequest;
    }
}
